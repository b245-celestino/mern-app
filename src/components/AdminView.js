import { useState, useEffect } from "react";
import { Table, Button, Modal, Form } from "react-bootstrap";
import Swal from "sweetalert2";

//IMAGES
import admindb from "../images/admindb.png";

export default function AdminView(props) {
  //destructure the productsProp and the fetchData function from Shop.js
  const { productsProp, fetchData } = props;
  // at the same time useEffect is monitoring this "state"
  const [productsArr, setProductsArr] = useState([]);
  const [productId, setProductId] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [picture, setPicture] = useState("");
  //states for handling modal visibility
  const [showEdit, setShowEdit] = useState(false);
  const [showCreate, setShowCreate] = useState(false);

  const token = localStorage.getItem("token");

  //OPEN-CLOSE MODAL FUNCTIONS (update)
  const openEdit = (productId) => {
    //console.log (productId)
    setShowEdit(true);

    // GOAL: autopopulate the modal for better U-ex
    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        //console.log(data)
        setProductId(data._id);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setPicture(data.image);
      });
  };

  const closeEdit = () => {
    setShowEdit(false);

    // good measure to prevent weird bugs
    setProductId("");
    setName("");
    setDescription("");
    setPicture("");
    setPrice(0);
  };

  // OPEN-CLOSE MODAL FUNCTIONS (create)
  const openCreate = () => {
    setShowCreate(true);
  };

  const closeCreate = () => {
    setShowCreate(false);

    setProductId("");
    setName("");
    setDescription("");
    setPicture("");
    setPrice(0);
  };

  // CREATE FUNCTION
  const createProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/product/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        image: picture,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          // alert("Product successfully created");
          Swal.fire({
            title: "Product Successfully Added!",
            icon: "success",
          });
          closeCreate();

          // key for hot reloading feature, we call fetchdata here to update the data we receive from the database.
          // calling fetchData, updates our coursesProp,which the useEffect below is monitoring
          // since courseProp updated, useEffect runs the code again which, re-renders our component
          fetchData();
        } else {
          alert("Something went wrong");
        }
      });
  };

  // UPDATE FUNCTION
  const editProduct = (e) => {
    e.preventDefault();

    fetch(
      `${process.env.REACT_APP_API_URL}/product/${productId}/updateproduct`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
          image: picture,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          // alert("Product successfully updated");

          Swal.fire({
            title: "Product Successfully Updated!",
            icon: "success",
          });
          closeEdit();

          // key for hot reloading feature, we call fetchdata here to update the data we receive from the database.
          // calling fetchData, updates our coursesProp,which the useEffect below is monitoring
          // since courseProp updated, useEffect runs the code again which, re-renders our component
          fetchData();
        } else {
          alert("Something went wrong");
        }
      });
  };

  // archive toggle function
  const archiveToggle = (productId, isAvailable) => {
    fetch(
      `${process.env.REACT_APP_API_URL}/product/${productId}/archiveproduct`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          isAvailable: !isAvailable,
          //!<element> reciprocates the result
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          let boolean;

          isAvailable ? (boolean = "disabled") : (boolean = "enabled");

          // alert(`Product successfully ${boolean}`);

          Swal.fire({
            title: `Product successfully ${boolean}`,
            icon: "success",
          });
          fetchData();
        } else {
          alert("Something went wrong");
        }
      });
  };

  // activate toggle function
  const activateToggle = (productId, isAvailable) => {
    fetch(
      `${process.env.REACT_APP_API_URL}/product/${productId}/activateproduct`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          isAvailable: !isAvailable,
          //!<element> reciprocates the result
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          let boolean;

          isAvailable ? (boolean = "disabled") : (boolean = "enabled");

          // alert(`Product successfully ${boolean}`);

          Swal.fire({
            title: `Product successfully ${boolean}`,
            icon: "success",
          });
          fetchData();
        } else {
          alert("Something went wrong");
        }
      });
  };

  useEffect(() => {
    //map through the productsProp to generate table contents
    //console.log(productsProp)
    const productsList = productsProp.map((product) => {
      return (
        <tr key={product._id}>
          <td>{product.name}</td>
          <td>{product.description}</td>
          <td>{product.price}</td>
          <td>
            {/*Dynamically render product availability*/}
            {product.isAvailable ? (
              <span>Available</span>
            ) : (
              <span>Unavailable</span>
            )}
          </td>
          <td>
            <center>
              <Button
                variant="info"
                size="sm"
                className="m-1"
                onClick={() => openEdit(product._id)}
              >
                Update
              </Button>
              {
                product.isAvailable ? (
                  //dynamically render which button show depending on product availability
                  <Button
                    variant="danger"
                    size="sm"
                    className="m-1"
                    onClick={() =>
                      archiveToggle(product._id, product.isAvailable)
                    }
                  >
                    Disable
                  </Button>
                ) : (
                  <Button
                    variant="success"
                    size="sm"
                    className="m-1"
                    onClick={() =>
                      activateToggle(product._id, product.isAvailable)
                    }
                  >
                    Enable
                  </Button>
                )
                // onClick calls the function archiveToggle thus making it run the fucntion
              }
            </center>
          </td>
        </tr>
      );
    });
    console.log(productsList);
    //set the CoursesArr state with the results of our mapping so that it can be used in the return statement
    setProductsArr(productsList);
  }, [productsProp]);

  return (
    <>
      <h1>
        <center>
          <img width="350px" className="my-5" src={admindb} />
        </center>
      </h1>

      <center>
        <Button variant="danger" size="sm" onClick={() => openCreate()}>
          Create New Product
        </Button>
      </center>

      {/*Product info table*/}
      <Table striped bordered hover responsive className="mt-3">
        <thead className="bg-dark text-white">
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody className="bg-white text-dark">
          {/*Mapped table contents dynamically generated from the productsProp*/}
          {productsArr}
        </tbody>
      </Table>

      {/* Create Product Modal*/}
      <Modal show={showCreate} onHide={closeCreate}>
        <Form onSubmit={(e) => createProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Create a new Product</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={name}
                onChange={(e) => setName(e.target.value)}
                type="text"
                required
              />
            </Form.Group>

            <Form.Group controlId="productImage">
              <Form.Label>Image</Form.Label>
              <Form.Control
                value={picture}
                onChange={(e) => setPicture(e.target.value)}
                type="text"
                required
              />
            </Form.Group>

            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                as="textarea"
                rows={3}
                required
              />
            </Form.Group>

            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                type="number"
                required
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeCreate}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>

      {/*Edit Product Modal*/}
      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={(e) => editProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Update Product</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={name}
                onChange={(e) => setName(e.target.value)}
                type="text"
              />
            </Form.Group>

            <Form.Group controlId="productImage">
              <Form.Label>Image</Form.Label>
              <Form.Control
                value={picture}
                onChange={(e) => setPicture(e.target.value)}
                type="text"
                required
              />
            </Form.Group>

            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                value={description}
                as="textarea"
                rows={3}
                onChange={(e) => setDescription(e.target.value)}
                type="text"
              />
            </Form.Group>

            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                type="number"
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}

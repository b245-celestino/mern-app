// IMPORT
//PACKAGES
import { useEffect, useState, useContext } from "react";
import { Col, Card, Row, Container } from "react-bootstrap";

//COMPONENTS
import OrderCard from "../components/OrderCard";
import UserContext from "../UserContext";

// IMAGES
import logotype from "../images/logotype.png";
import ordersSum from "../images/ordersSum.png";

// FUNCTION EXPORT
export default function () {
  const [orderData, setOrderData] = useState([]);

  const { user } = useContext(UserContext);
  const token = localStorage.getItem("token");

  // fetched data from backend and set orderData using useState
  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/order/retrieve`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrderData(data);
      });
  };

  // fetch the data upon component mount
  useEffect(() => {
    //console.log(process.env.REACT_APP_API_URL)
    // changes to env files are applied only at build time (when starting the project locally)
    fetchData();
  }, []);

  // map through the orderData
  const orders = orderData.map((order) => {
    return <OrderCard ordersProp={order} key={order._id} />;
  });
  return (
    <>
      <Container className="justify-content-center py-5">
        <Row>
          <center>
            <img width="350px" src={logotype} />
          </center>
        </Row>
      </Container>
      <center>
        <img width="350px" src={ordersSum} />
      </center>
      <Container fluid>
        <Row className="mt-5 d-flex">{orders}</Row>
      </Container>
    </>
  );
}

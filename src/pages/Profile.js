import React from "react";
import { useState, useEffect, useContext } from "react";
import { Container, Row, Col, Card, ListGroup, Button } from "react-bootstrap";
import UserContext from "../UserContext";
import ProfileCard from "../components/ProfileCard";
import OrderCard from "../components/OrderCard";

export default function () {
  const [profileData, setProfileData] = useState([]);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { user } = useContext(UserContext);
  const token = localStorage.getItem("token");

  // fetched data from backend and set orderData using useState
  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
        setPassword(data.password);
      });
  };

  console.log(profileData);

  // fetch the data upon component mount
  useEffect(() => {
    //console.log(process.env.REACT_APP_API_URL)
    // changes to env files are applied only at build time (when starting the project locally)
    fetchData();
  }, []);

  // map through the orderData
  const profiles = profileData.map((order) => {
    return <ProfileCard profilesProp={order} key={order._id} />;
  });
  return (
    <Container className="text-center  ">
      <Row>
        <Col className="d-flex justify-content-center">
          <Card className="mt-5" style={{ width: "18rem" }}>
            <Card.Img variant="top" src="https://via.placeholder.com/150" />
            <Card.Body>
              <Card.Title>
                {firstName} {lastName}
              </Card.Title>
            </Card.Body>
            <ListGroup className="list-group-flush">
              <ListGroup.Item>
                Email: <em>{email}</em>{" "}
              </ListGroup.Item>
              {/* <ListGroup.Item>{password}</ListGroup.Item> */}
            </ListGroup>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

// IMPORTS
import { Form, Button, Container, Row, Col, Alert } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Redirect, Link, useNavigate } from "react-router-dom";

import register from "../images/register.gif";
import logotype from "../images/logotype.png";
import Swal from "sweetalert2";
import { Navigate } from "react-router-dom";
import retrieveUserDetails from "../pages/Login";

export default function Register(props) {
  //console.log(props)

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");

  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  const { user, setUser } = useContext(UserContext);

  // duplicate email
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const navigate = useNavigate();
  /*
		To properly change and input values, we must implement two-way binding. (value & onChange)

		We need to capture whatever the user types in the input as they are typing

		Meaning we need the input's .value

		To get the .value, we need to capture the event (in this case, onChange). The target of the onChange is the input, meaning we can get the .value
	*/

  /*mini-project change password visibility*/

  /* USE EFFECT (useEffect)

		Syntax:
			useEffect (() =>
			{
				<code to be executed>,
				[<state/s> (to be monitored)]
			})

	*/

  useEffect(() => {
    // console.log (email)
    // console.log (password1)
    // console.log (password2)

    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, password1, password2]);

  function registerUser(e) {
    e.preventDefault(); // prevent default form behaviour
    //alert (`Thanks for registering ${email}!`)

    const retrieveUserDetails = (token) => {
      console.log(token);
      fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          // setUser to these values
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
            email: data.email,
          });
        });
    };

    // fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify({
    //     email: email,
    //   }),
    // })
    //   .then((res) => res.json())
    //   .then((data) => {
    //     if (data) {
    //       handleShow();
    //     } else {
    fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Registration Successful!",
            icon: "success",
            text: `Thanks for registering ${firstName}!`,
          });

          navigate("/login");

          // props.history.push("/login");
        } else {
          // localStorage.setItem("token", data.auth);
          // retrieveUserDetails(localStorage.getItem("token"));
          Swal.fire({
            title: "Registration Failed!",
            icon: "error",
            text: "The email is already existed!",
          });
        }
      });
  }

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <Container className="mt-5">
      <Row>
        <Col lg={6}>
          {/* {show ? (
            <Alert variant="danger" onClose={() => setShow(false)} dismissible>
              <Alert.Heading>
                A duplicate e-mail address already exists in our system
              </Alert.Heading>
              <p>
                Do you want to{" "}
                <Link className="text-dark" to={"/login"}>
                  <strong>log-in</strong>{" "}
                </Link>{" "}
                instead?
              </p>
            </Alert>
          ) : (
            <></>
          )} */}
          <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userFirstName">
              <Form.Label className="mt-3">
                <strong>First Name</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter first name"
                onChange={(e) => setFirstName(e.target.value)}
                value={firstName}
                required
              />
            </Form.Group>
            <Form.Group controlId="userLastName">
              <Form.Label className="mt-3">
                <strong>Last Name</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter last name"
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
                required
              />
            </Form.Group>

            <Form.Group controlId="userEmail">
              <Form.Label className="mt-3">
                <strong>Email Address</strong>
              </Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                required
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
              <Form.Label className="mt-3">
                <strong>Password</strong>
              </Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter password"
                onChange={(e) => setPassword1(e.target.value)}
                value={password1}
                required
              />
            </Form.Group>

            <Form.Group controlId="password2">
              <Form.Label className="mt-3">
                <strong>Verify Password</strong>
              </Form.Label>
              <Form.Control
                type="password"
                placeholder="Verify password"
                onChange={(e) => setPassword2(e.target.value)}
                value={password2}
                required
              />
            </Form.Group>

            {isActive ? (
              <Button
                className="mt-3"
                variant="primary"
                type="submit"
                id="button"
              >
                Submit
              </Button>
            ) : (
              <Button className="mt-3" variant="dark" id="button" disabled>
                Submit
              </Button>
            )}
          </Form>
        </Col>

        <Col lg={6}>
          <h3 id="welcomeText" className="mt-3 text-center">
            Welcome to{" "}
          </h3>
          <center>
            <img width="50%" className="img-fluid " src={logotype} />
          </center>
          <center>
            <img className="img-fluid" src={register} />
          </center>
        </Col>
      </Row>
    </Container>
  );
}

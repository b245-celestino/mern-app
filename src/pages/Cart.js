// IMPORT
//PACKAGES
import { useEffect, useState, useContext } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Modal, Form, Container } from "react-bootstrap";
import { Redirect } from "react-router-dom";

// IMAGES
import empty from "../images/empty.png";
import cartEmpty from "../images/cartEmpty.gif";
import successCart from "../images/success.png";
import cartcontents from "../images/cartcontents.png";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import CartContext from "../CartContext";

export default function Cart(props) {
  const { user, setUser } = useContext(UserContext);
  //STATES THAT YOU MAY WANT TO HAVE:
  //set state to get the grand total of all subtotals
  const [total, setTotal] = useState(0);
  //set state for cart
  const [cart, setCart] = useState([]);
  const [cartArr, setCartArr] = useState([]);
  // const { cartArr, setCartArr } = useContext(CartContext);

  const [cartTotal, setCartTotal] = useState([]);

  //states to determine the current product the user can add to their cart
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [qty, setQty] = useState(1);
  const [price, setPrice] = useState(0);
  const [picture, setPicture] = useState("");

  //added an order
  //added to cart
  const [showAdd, setShowAdd] = useState(false);

  const { badge, setBadge } = useContext(CartContext);
  useEffect(() => {
    // console.log(cartArr);
    if (localStorage.getItem("cart")) {
      setCartArr(JSON.parse(localStorage.getItem("cart")));
      setBadge(cartArr.length);
    }
  }, [cartArr]);

  //FOR GETTING YOUR LOCALSTORAGE CART ITEMS AND SETTING YOUR CART STATE:
  //on component mount, check if there is an existing cart in localStorage. If there is, set its contents as our cart state

  useEffect(() => {
    if (localStorage.getItem("cart")) {
      setCart(JSON.parse(localStorage.getItem("cart")));
    }
  }, []);

  // MAP THROUGH CART TO GENERATE TABLE
  useEffect(() => {
    if (localStorage.getItem("cart")) {
      const cartItems = JSON.parse(localStorage.getItem("cart"));
      console.log(cartItems);

      const cartList = cartItems.map((product) => {
        return (
          <tr key={product.productId}>
            <td>
              <img width="50px" src={product.image} />
            </td>
            <td>
              <Link className="text-dark" to={`/shop/${product.productId}`}>
                {" "}
                {product.name}
              </Link>
            </td>
            <td>{product.price}</td>
            <td>
              <div class="d-none d-md-flex w-100 input-group">
                <div class="input-group-prepend">
                  <button
                    type="button"
                    class="btn btn-secondary"
                    onClick={(e) => decQty(product.productId, product.qty)}
                  >
                    -
                  </button>
                </div>
                <input
                  min="1"
                  type="number"
                  class="form-control"
                  value={product.quantity}
                  onChange={(e) => qtyInput(product.productId, e.target.value)}
                ></input>
                <div class="input-group-append">
                  <button
                    type="button"
                    class="btn btn-secondary"
                    onClick={(e) => incQty(product.productId, product.qty)}
                  >
                    +
                  </button>
                </div>
              </div>
            </td>
            <td>{product.subtotal}</td>
            <td>
              <center>
                <button
                  type="button"
                  class="btn btn-danger"
                  onClick={(productId) => removeItem(product.productId)}
                >
                  Remove
                </button>
              </center>
            </td>
          </tr>
        );
      });

      //set the CoursesArr state with the results of our mapping so that it can be used in the return statement
      setCart(cartList);
      console.log(cart);
    }
  }, [cartArr]);

  //FOR UPDATING A STATE THAT DETERMINES THE TOTAL AMOUNT:
  //whenever our cart state changes, re-calculate the total
  useEffect(() => {
    const cartItems = JSON.parse(localStorage.getItem("cart"));
    //console.log(cartItems)
    setCartTotal(cartItems);

    //start with a counter initialized to zero
    let tempTotal = 0;

    //loop through our cart, getting each item's subtotal and incrementing our tempTotal counter by its amount
    cartTotal.forEach((product) => {
      tempTotal += product.subtotal;
    });

    //set our total state
    setTotal(tempTotal);
  }, [cart, cartArr]);

  function addToCart(e) {
    //variable to determine if the item we are adding is already in our cart or not
    let alreadyInCart = false;
    //variable for the item's index in the cart array, if it already exists there
    let productIndex;
    //temporary cart array
    let cart = [];

    if (localStorage.getItem("cart")) {
      cart = JSON.parse(localStorage.getItem("cart"));
    }

    //loop through our cart to check if the item we are adding is already in our cart or not
    for (let i = 0; i < cart.length; i++) {
      if (cart[i].productId === id) {
        //if it is, make alreadyInCart true
        alreadyInCart = true;
        productIndex = i;
      }
    }

    //if a product is already in our cart, just increment its quantity and adjust its subtotal
    if (alreadyInCart) {
      cart[productIndex].quantity += qty;
      cart[productIndex].subtotal =
        cart[productIndex].price * cart[productIndex].quantity;
    } else {
      //else add a new entry in our cart, with values from states that need to be set wherever this function goes
      cart.push({
        productId: id,
        name: name,
        price: price,
        quantity: qty,
        subtotal: price * qty,
        image: picture,
      });
    }

    //set our localStorage cart as well
    localStorage.setItem("cart", JSON.stringify(cart));
    console.log(cart);
  }

  //FOR ADJUSTING THE QUANTITY OF AN ITEM BASED ON INPUT:
  const qtyInput = (productId, value) => {
    //use the spread operator to create a temporary copy of our cart (from the cart state)
    let tempCart = JSON.parse(localStorage.getItem("cart"));
    console.log(tempCart);
    //loop through our tempCart
    for (let i = 0; i < tempCart.length; i++) {
      //so that we can find the item with the quantity we want to change via its productId
      if (tempCart[i].productId === productId) {
        //use parseFloat to make sure our new quantity will be parsed as a number
        tempCart[i].quantity = parseFloat(value);
        //set the new subtotal
        tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity;
      }
    }
    console.log(tempCart);

    //set our cart state with the new quantities
    setCartArr(tempCart);

    //set our localStorage cart as well
    localStorage.setItem("cart", JSON.stringify(tempCart));
  };

  // FOR INCREMENT BUTTON
  const incQty = (productId, qty) => {
    //use the spread operator to create a temporary copy of our cart (from the cart state)
    let tempCart = JSON.parse(localStorage.getItem("cart"));
    console.log(tempCart);
    //loop through our tempCart
    for (let i = 0; i < tempCart.length; i++) {
      //so that we can find the item with the quantity we want to change via its productId
      if (tempCart[i].productId === productId) {
        //use parseFloat to make sure our new quantity will be parsed as a number
        tempCart[i].quantity += 1;
        //set the new subtotal
        tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity;
      }
    }

    console.log(tempCart);

    //set our cart state with the new quantities
    setCartArr(tempCart);

    //set our localStorage cart as well
    localStorage.setItem("cart", JSON.stringify(tempCart));
  };

  // FOR DECREMENT BUTTON
  const decQty = (productId, qty) => {
    //use the spread operator to create a temporary copy of our cart (from the cart state)
    let tempCart = JSON.parse(localStorage.getItem("cart"));
    console.log(tempCart);
    //loop through our tempCart
    for (let i = 0; i < tempCart.length; i++) {
      //so that we can find the item with the quantity we want to change via its productId
      if (tempCart[i].productId === productId) {
        //use parseFloat to make sure our new quantity will be parsed as a number
        tempCart[i].quantity -= 1;
        //set the new subtotal
        tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity;
      }
    }
    console.log(tempCart);

    //set our cart state with the new quantities
    setCartArr(tempCart);

    //set our localStorage cart as well
    localStorage.setItem("cart", JSON.stringify(tempCart));
  };

  //FOR REMOVING AN ITEM FROM CART:
  const removeItem = (productId) => {
    //use the spread operator to create a temporary copy of our cart (from the cart state)
    console.log(productId);

    const cartItems = JSON.parse(localStorage.getItem("cart"));

    //console.log(cartItems)

    // let tempCart = [...cartItems]
    // console.log(tempCart) // not this

    //console.log(cartItems[0])
    // get the correct index of matching product ID
    let indexToDelete = cartItems.findIndex(function (product) {
      return product.productId === productId;
    });
    console.log(indexToDelete);
    //console.log(cartItems.indexOf(productId))
    //use splice to remove the item we want from our cart
    cartItems.splice([indexToDelete], 1);
    console.log(cartItems);

    //set our cart state with the new quantities
    // setCart(JSON.stringify(cartItems))
    console.log(cart);
    setCartArr(cartItems);
    console.log(cartArr);

    //set our localStorage cart as well
    localStorage.setItem("cart", JSON.stringify(cartItems));
  };

  // CHECKOUT FUNCTION
  const checkOutFunc = (e) => {
    console.log(typeof total);
    let cartCheckOut = JSON.parse(localStorage.getItem("cart"));
    console.log(cartCheckOut);
    const token = localStorage.getItem("token");
    console.log(token);
    fetch(`${process.env.REACT_APP_API_URL}/order/sample`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        products: cartCheckOut,
        totalAmount: total,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          // alert(`You have successfully placed an order!`);
          setShowAdd(true);
          props.history.push("/orders");

          localStorage.removeItem("cart");
        } else {
          alert("Oops! Something went wrong!");
        }
      });
  };

  return user.isAdmin ? (
    <Navigate to="/*" />
  ) : localStorage.getItem("cart") ? (
    <>
      <center>
        <img className="my-5" width="375px" src={cartcontents} />
      </center>

      {/*Course info table*/}
      <Table striped bordered hover responsive>
        <thead className="bg-dark text-white">
          <tr>
            <th>Item</th>
            <th>Name</th>
            <th>Price</th>
            <th class="w-25">Quantity</th>
            <th>Subtotal</th>
            <th>Remove</th>
          </tr>
        </thead>
        <tbody>
          {/*Mapped table contents dynamically generated from the coursesProp*/}
          {cart}
          <tr>
            {total === 0 ? (
              <th colspan="5">
                {" "}
                <center>
                  <button type="button" class="btn btn-success" disabled>
                    Add items to your cart first
                  </button>
                </center>
              </th>
            ) : (
              <th colspan="5">
                {" "}
                <center>
                  <button
                    id="button"
                    type="button"
                    class="btn btn-success"
                    // onClick={() => setShowAdd(true)}
                    onClick={(e) => checkOutFunc(e)}
                  >
                    Proceed to Checkout
                  </button>
                </center>
              </th>
            )}
            <th id="total">TOTAL: ₱{total} </th>
          </tr>
        </tbody>
      </Table>

      <Modal
        className="text-center justify-content-center"
        centered
        show={showAdd}
        animation={true}
      >
        <Modal.Body>
          <strong>
            <p>Successfully placed an order!</p>
          </strong>
          <center>
            <img width="100px" src={successCart} />
          </center>
          <Link className="nav-link mx-2 mt-3" to="/">
            <Button variant="outline-dark">Go to Homepage </Button>
          </Link>

          <Link className="nav-link mx-2 mt-3" to="/orders">
            <Button variant="outline-dark">Go to Orders </Button>
          </Link>

          {/* <button
            className="btn-danger btn mt-3"
            // onClick={(e) => checkOutFunc(e)}
          >
            Proceed to Orders
          </button> */}
        </Modal.Body>
      </Modal>
    </>
  ) : (
    <>
      <Container className="container-fluid">
        <div className="d-flex justify-content-center">
          <img width="500px" className="mt-5 img-fluid" src={empty} />
        </div>

        <center>
          <img className="mb-3 img-fluid" src={cartEmpty} />
        </center>

        <div>
          <p className="text-center m-3">
            <Link className="nav-link" to="/shop">
              <Button id="button" variant="dark">
                Start Shopping Now!{" "}
              </Button>
            </Link>
          </p>
        </div>
      </Container>
    </>
  );
}

// import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';

import logo from "../images/logo.png";
import header from "../images/header1.png";

import featured from "../images/featured.png";

import featured1 from "../images/products/featured1.jpg";
import featured2 from "../images/products/featured2.jpg";
import featured3 from "../images/products/featured3.jpg";
import featured4 from "../images/products/featured4.jpg";

import { Link, NavLink } from "react-router-dom";
import { Carousel, Card, Container, Row, Col } from "react-bootstrap";

export default function Home() {
  return (
    <>
      <Container fluid className="justify-content-center ">
        <Row>
          <img className="img-fluid" src={header} />
        </Row>
      </Container>

      <Carousel className="mt-lg-5 " id="featCarousel">
        <Carousel.Item interval={6500}>
          <img
            className="d-block w-100"
            src="https://images.unsplash.com/photo-1588580000645-4562a6d2c839?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
            alt="First slide"
            id="carousel"
          />
        </Carousel.Item>

        <Carousel.Item interval={6500}>
          <img
            className="d-block w-100"
            src="https://images.unsplash.com/photo-1618365908648-e71bd5716cba?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
            alt="Second slide"
            id="carousel"
          />
        </Carousel.Item>

        <Carousel.Item interval={6500}>
          <img
            className="d-block w-100"
            src="https://images.unsplash.com/photo-1623771702313-39dc4f71d275?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
            alt="Third slide"
            id="carousel"
          />
        </Carousel.Item>
      </Carousel>

      <center>
        {/* <img className="mt-5 img-fluid" width="330px" src={featured} /> */}
        <h2>Featured Books</h2>
      </center>

      <Carousel className="mt-1 p-3">
        <Carousel.Item interval={6500}>
          <Container>
            <Row>
              <Col xs={6} md={3}>
                <div className="imgCont">
                  <Link
                    className="nav-link mt-1 text-center"
                    to="/shop/63f59bc18f51ee176b057ac4"
                  >
                    <img
                      className="d-block w-100"
                      src={featured1}
                      alt="Second slide"
                      id="featuredProd"
                    />
                  </Link>
                </div>
              </Col>
              <Col xs={6} md={3}>
                <div className="imgCont">
                  <Link
                    className="nav-link mt-1 text-center"
                    to="/shop/63f59ced8f51ee176b057ae0"
                  >
                    <img
                      className="d-block w-100"
                      src={featured2}
                      alt="Second slide"
                      id="featuredProd"
                    />
                  </Link>
                </div>
              </Col>
              <Col xs={6} md={3}>
                <div className="imgCont">
                  <Link
                    className="nav-link mt-1 text-center"
                    to="/shop/63f59c1d8f51ee176b057ad2"
                  >
                    <img
                      className="d-block w-100"
                      src={featured3}
                      alt="Second slide"
                      id="featuredProd"
                    />
                  </Link>
                </div>
              </Col>
              <Col xs={6} md={3}>
                <div className="imgCont">
                  <Link
                    className="nav-link mt-1 text-center"
                    to="/shop/63f59c738f51ee176b057ad6"
                  >
                    <img
                      className="d-block w-100"
                      src={featured4}
                      alt="Second slide"
                      id="featuredProd"
                    />
                  </Link>
                </div>
              </Col>
            </Row>
          </Container>
        </Carousel.Item>
      </Carousel>
    </>
  );
}
